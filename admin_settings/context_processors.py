
"""
	put the admin settings marked as such into the context
"""

from admin_settings.models import Setting
from django.db.utils import OperationalError


def admin_settings(request):
	template_settings = {}
	try:
		for setting in Setting.objects.all():
			if setting.template >= 0:
				template_settings[setting.name] = setting.value
	except OperationalError:
		print(('%s could not query Setting; did you run syncdb?' % __file__))
		return {}
	return dict(template_settings)


