# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from admin_settings import Setting as LatestSetting
from django.db import migrations
from os import environ
from sys import stderr



def initial_settings(apps, schema_editor):
	Setting = apps.get_model('admin_settings', 'Setting')  # get the historical version, not the latest one
	if Setting.objects.count():
		""" settings are already installed """
		return
	if not 'DJANGO_SETTINGS_MODULE' in environ:
		stderr.write('default settings for admin_settings were not installed because DJANGO_SETTINGS_MODULE was not found in os.environ\n')
		return
	Setting(
		name = 'DEMO_SETTING',
		type = LatestSetting.INT,
		value_str = 42,
		explanation = 'This is just a demo setting, it probably does not influence the site in any way. Go ahead and replace it with your own settings!',
		template = True,
		sensitive = False,
	).save()


class Migration(migrations.Migration):

	dependencies = [
		('admin_settings', '0001_initial'),
	]

	operations = [
		migrations.RunPython(initial_settings),
	]


