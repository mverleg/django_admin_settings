# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('admin_settings', '0002_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='setting',
            name='type',
            field=models.CharField(default=b'str', help_text=b'The type of value of this setting (text, number, etc)', max_length=8, choices=[(b'int', b'integer number'), (b'float', b'real number'), (b'templ', b'django template'), (b'json', b'list or dictionary (json)'), (b'bool', b'boolean (true/false)'), (b'str', b'text')]),
            preserve_default=True,
        ),
    ]
