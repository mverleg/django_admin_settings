# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=32, help_text='The key for this setting as it is used in code', unique=True, validators=[django.core.validators.RegexValidator(code='Invalid name', regex='^[a-zA-Z][a-zA-Z0-9_]*$', message='Only alphanumeric names starting with a letter')])),
                ('type', models.CharField(max_length=8, help_text='The type of value of this setting (text, number, etc)', choices=[('templ', 'django template'), ('json', 'list or dictionary (json)'), ('int', 'integer number'), ('float', 'real number'), ('bool', 'boolean (true/false)'), ('str', 'text')], default='str')),
                ('value_str', models.TextField(blank=True, null=True, verbose_name='value', help_text='The current value of this setting', db_column='value')),
                ('explanation', models.TextField(blank=True, help_text='Description of what this setting means', default='')),
                ('template', models.PositiveSmallIntegerField(help_text='Indicated whether this value should be made available in templates', choices=[(0, 'not in templates'), (1, 'in templates auto-escaped'), (2, 'in templates not escaping')], default=0)),
                ('sensitive', models.BooleanField(help_text='Indicates whether changing this value is likely to break things', default=False)),
            ],
            options={
                'verbose_name': 'Setting',
                'permissions': (('change_setting_sentitive_values', 'Can change sensitive Setting'), ('change_setting_properties', 'Can change all properties')),
            },
            bases=(models.Model,),
        ),
    ]
