
from django.db.utils import OperationalError
from admin_settings.models import Setting

__all__ = ['Setting']

try:
    all_settings =  dict((str(setting.name), setting.value) for setting in Setting.objects.all())
except OperationalError:
    print(('%s could not query Setting; did you run syncdb?' % __file__))
else:
    __all__ += list(all_settings.keys())
    locals().update(all_settings)


