
from django.db import models
from django.template import Template, Context
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.safestring import mark_safe
from admin_settings import type_functions
from admin_settings.type_functions import types_names, convert_to, to_storage

types = list(types_names.items())
PERM_SENSITIVE  = 'change_setting_sentitive_values'
PERM_PROPERTIES = 'change_setting_properties'

TEMPLATE_OPTIONS = (
	(0, 'not in templates'),
	(1, 'in templates auto-escaped'),
	(2, 'in templates not escaping'),
)

varname_validator = RegexValidator(regex = r'^[a-zA-Z][a-zA-Z0-9_]*$', message = 'Only alphanumeric names starting with a letter', code = 'Invalid name')


class Setting(models.Model):

	name = models.CharField(max_length = 32, unique = True, validators = [varname_validator], help_text = 'The key for this setting as it is used in code')
	type = models.CharField(max_length = 8, choices = types, default = type_functions.STR, help_text = 'The type of value of this setting (text, number, etc)')
	value_str = models.TextField(blank = True, null = True, verbose_name = 'value', help_text = 'The current value of this setting', db_column = 'value')
	explanation = models.TextField(blank = True, default = '', help_text = 'Description of what this setting means')
	template = models.PositiveSmallIntegerField(default = 0, choices = TEMPLATE_OPTIONS, help_text = 'Indicated whether this value should be made available in templates')
	sensitive = models.BooleanField(default = False, help_text = 'Indicates whether changing this value is likely to break things')

	note = '[value dynamically overwritten by SettingAdmin.get_form]'
	STR, BOOL, INT, FLOAT, JSON, TEMPL = type_functions.STR, type_functions.BOOL, type_functions.INT, type_functions.FLOAT, type_functions.JSON, type_functions.TEMPL

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = 'Setting'
		permissions = (
			(PERM_SENSITIVE, 'Can change sensitive %s' % verbose_name),
			(PERM_PROPERTIES, 'Can change all properties'),
		)

	@property
	def value(self):
		val = convert_to(self.value_str, self.type)
		if self.type == self.TEMPL:
			val = Template(val).render(Context())
		if self.template == 2 and (self.type in (self.STR, self.TEMPL,)):
			val = mark_safe(val)
		return val

	'''
		this does not save the setting unless you explicitly call save
	'''
	@value.setter
	def value(self, val):
		self.value_str = to_storage(val, self.type)

	'''
		check for the special permissions defined in Meta:
		PERM_SENSITIVE: ability to also edit sensitive fields
		PERM_PROPERTIES: ability to change properties other than value
	'''
	@staticmethod
	def has_sensitive_value_perm(request, *args, **kwargs):
		return request.user.has_perm('admin_settings.%s' % PERM_SENSITIVE)

	@staticmethod
	def has_property_perm(request, *args, **kwargs):
		return request.user.has_perm('admin_settings.%s' % PERM_PROPERTIES)

	'''
		makes sure that the value matches the type by trying to convert it
		(the converted value isn't actually stored as such; everything is
		stored as a string)
	'''
	def clean(self):
		try:
			convert_to(self.value, self.type)
		except ValueError as err:
			raise ValidationError(str(err))

	'''
		makes sure that the value matches the type by converting it, then
		converting is back to unicode because that's how everything is stored
	'''
	def save(self, *args, **kwargs):
		''' just making sure that it can be converted '''
		self.value = to_storage(convert_to(self.value, self.type), self.type)
		super(Setting, self).save(*args, **kwargs)


