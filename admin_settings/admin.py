
from django.contrib import admin
from admin_settings.models import Setting


class SettingAdmin(admin.ModelAdmin):
    '''
        customize the settings overview page
    '''
    search_fields = ['name', 'explanation', 'value_str']
    list_display = ['name', 'value_str', 'type', 'explanation', 'template', 'sensitive']
    actions = None
    
    '''
        the fields that are displayed and editable as well as the notice at the 
        bottom depend heavily on the permissions of the user and on whether 
        it's a sensitive field; this function takes care of all that
    '''
    def get_form(self, request, obj = None, *args, **kwargs):
        access_restricted = True
        sensitive = obj.sensitive if obj else False
        name = obj.name if obj else 'this setting'
        note = ''
        if not sensitive:
            access_restricted = False
        elif Setting.has_sensitive_value_perm(request, *args, **kwargs):
            access_restricted = False
        if Setting.has_property_perm(request, *args, **kwargs):
            note = 'Changing the name or type of %s is likely to cause the site to misbehalve, as these are possibly relied upon by code. Only edit these properties if you know what you are doing.\n' % name
            if sensitive:
                if not access_restricted:
                    note += '%s is a sensitive setting, which can stop the site from functioning properly if not set correctly. Please edit it carefully.' % name
            self.fields = ['name', 'explanation', 'value_str', 'type', 'template', 'sensitive']
            if access_restricted:
                note = 'you are allowed to '
                self.readonly_fields = ['name', 'explanation', 'value_str', 'type', 'template']
            else:
                self.readonly_fields = []
        else:
            note = ''
            self.fields = ['name', 'value_str', 'type', 'template']
            if access_restricted:
                self.readonly_fields = self.fields
            else:
                self.readonly_fields = ['name', 'explanation', 'type', 'template']
            if obj.explanation:
                self.fields.insert(1, 'explanation')
        if sensitive and access_restricted:
            note += 'You do not have permission to edit %s because it is a sensitive setting, which can stop the site from functioning properly if not set correctly' % name  
        if len(note) and obj:
            obj.note = note
            self.fields.append('note')
            self.readonly_fields.append('note')
        return super(SettingAdmin, self).get_form(request, obj = obj, *args, **kwargs)
    
    #'''
    #    the normal add and delete permissions are overruled and have no effect; 
    #    being able to add properties other than value is the only and sufficient
    #    condition for adding and removing settings.
    #'''
    #def has_add_permission(self, request, *args, **kwargs):
    #    return Setting.has_property_perm(request, *args, **kwargs)
    
    #def has_delete_permission(self, request, *args, **kwargs):
    #    return Setting.has_property_perm(request, *args, **kwargs)
    
    '''
        the normal change permission let's users change the value of non-sensitive
        settings; having the special permission to change sensitive items or all
        attributes implies the permission to change non-sensitive items 
    '''
    def has_change_permission(self, request, *args, **kwargs):
        return super(SettingAdmin, self).has_change_permission(request, *args, **kwargs) or \
            Setting.has_sensitive_value_perm(request, *args, **kwargs) or \
            Setting.has_property_perm(request, *args, **kwargs)


admin.site.register(Setting, SettingAdmin)


