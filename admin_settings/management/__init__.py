
from django.db.models.signals import post_syncdb
from admin_settings.management.syncdb import post_syncdb_callback


'''
    register post_syncdb for creating default settings
'''
post_syncdb.connect(post_syncdb_callback)

