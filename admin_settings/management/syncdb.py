
from os import environ
from sys import stderr
from admin_settings.models import Setting
from admin_settings.type_functions import INT # that's a shame, hard-code app name...


'''
    install default data when this app is installed
'''
def post_syncdb_callback(sender, *args, **kwargs):
    if Setting.objects.count():
        ''' settings are already installed '''
        return
    if not 'DJANGO_SETTINGS_MODULE' in environ:
        stderr.write('default settings for admin_settings were not installed because DJANGO_SETTINGS_MODULE was not found in os.environ\n')
        return
    
    Setting(
        name = 'DEMO_SETTING',
        type = INT,
        value = 42,
        explanation = 'This is just a demo setting, it probably does not influence the site in any way. Go ahead and replace it with your own settings!',
        template = True,
        sensitive = False,
    ).save()


