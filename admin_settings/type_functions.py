
from json import dumps, loads

STR, BOOL, INT, FLOAT, JSON, TEMPL = 'str', 'bool', 'int', 'float', 'json', 'templ'

types_names = {
    STR:   'text',
    BOOL:  'boolean (true/false)',
    INT:   'integer number',
    FLOAT: 'real number',
    JSON:  'list or dictionary (json)',
	TEMPL: 'django template',
}

'''
    given a string, try to convert to the correct type
    otherwise: ValueError
'''
def convert_to(str_value, type_name):
    if str_value == None:
        return None
    if type_name in (STR, TEMPL,):
        return str(str_value)
    if type_name == BOOL:
        try:
            return bool(int(str_value))
        except:
            raise ValueError('use a 0 to indicate false or a 1 to indicate true')
    if type_name == INT:
        return int(float(str_value))
    if type_name == FLOAT:
        return float(str_value)
    if type_name == JSON:
        return loads(str_value)
    raise Exception('type "%s" not known' % type_name)

'''
    convert to unicode for storage
    (type_name could be determined automatically,
    but why bother)
'''
def to_storage(value, type_name):
    if value == None:
        return None
    if type_name == JSON:
        return dumps(value)
    if type_name == BOOL:
        value = int(value)
    return str(value)

