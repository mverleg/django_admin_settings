
from django import template
from admin_settings.models import Setting

register = template.Library()


'''
    this tag displays an admin_setting (if available in templates):
        {% admin_setting 'SETTING_NAME' %}
    not that usually you can just display them by 
        {{ SETTING_NAME }}
    but this tag is available for cases like admin without request context
'''
@register.simple_tag(name = 'admin_setting')
def admin_setting(setting_name):
    try:
        setting = Setting.objects.get(name = setting_name)
    except Setting.DoesNotExist:
        raise Exception('no admin setting named \'%s\' found' % setting_name)
    if not setting.template:
        raise Exception('admin setting named \'%s\' not available in templates' % setting_name)
    return setting.value


