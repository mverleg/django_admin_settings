
Django Admin Settings
-----------

Django Admin Settings lets you put (some) site-wide settings for Django in the database and makes them editable easily through the admin interface.

Installation & Configuration:
-----------

- Install using ``pip install git+https://mverleg@bitbucket.org/mverleg/django_admin_settings.git`` (or download and copy the app into your project)
- Include ``admin_settings`` in ``INSTALLED_APPS``
- Include ``'admin_settings.context_processors.admin_settings'`` in ``TEMPLATE_CONTEXT_PROCESSORS``, if you want to use settings in templates
- Run ``manage.py syncdb``

Use in admin
-----------

Simply go to /admin/ and select the settings model. See 'permissions' to select authorize users for specific edits.

Use in templates
-----------

You can display variables by name in templates that have RequestContext

                {{ DEMO_SETTING }}

This will only work if the specific setting is marked as displayable in templates.

Use in code
-----------
Include any admin settings like this:

                from admin_settings include DEMO_SETTING

Usually that's all there is to it. You can now use and change this setting without changing the database value.

Might you want to create or edit settings from your code, you can do something like

                demo_setting = Setting.objects.get(name = 'DEMO_SETTING')
                
                # maybe catch the possible Setting.DoesNotExist
                
                demo_setting.value = 37
                
                demo_setting.template = True
                
                demo_setting.save()

But in many cases it is enough to change settings in the admin interface.

Settings
-----------

Settings have these properties:

- name          the name of the settings as imported, e.g. DEMO_SETTING (caps are conventional, not required)
- type          the type of the setting, e.g. text, int or float; complex structures can be stored as json and will be loaded automatically
- value         the value of the setting; normal change permissions allow only to change this (e.g. change ADMIN_NAME for display but not break all your imports by renaming it)
- explanation   an optional explanation of what the setting does
- template      should the setting be made available in all template contexts? (useful for e.g. admin contact information, project slogan etc)
- sensitive     if the setting is marked as sensitive, special permission is needed to change it's value

Permissions
-----------

Staff members can be given fairly specific permissions to change settings

- change                user may change the value of settings not marked as sensitive (e.g., you may let someone change display features but not things vital for the functioning of the site)
- change vital          users may change the value of any settings
- change properties     users may change any properties, including the name and type of settings
- add/delete            add and delete settings, obviously

Performance
-----------

A way to cache database queries is recommended (in general, but it should work really well for admin_settings' high lookup ratio).

Without template context, admin_settings as used in code will only query on server reload (from ``__init__.py``). With template context there is one additional query on every request (that uses a template).

When to use
-----------

Database settings are intended for website settings such as administrator names, available modes of registration or a default theme. The things that can be changed by the website operator, who does not (necessarily) know anything about Django.

They are not intended for software-specific technical settings such as media locations, cache backends or database settings. These things should be changed by the creator of the website (you, presumably) in the normal settings file.

License
-----------

django_admin is available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use my name for promotion and are aware that there is no warranty.


